<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="17"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_aboutdialog.h" line="80"/>
        <source>About This Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="27"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_aboutdialog.h" line="81"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans Fallback&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This application is a lightweight addon manager for TESO. Simply a bit faster than its Java big brother. Created and tested on Ubuntu Linux for those playing TESO using WINE.&lt;br /&gt;Author: @esorochinskiy&lt;br /&gt;Special thanks to:&lt;br /&gt;Good Game Mods, LLC&lt;br /&gt;&lt;br /&gt;The software is distributed under &lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;GPL V3&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Source code is available on &lt;a href=&quot;https://gitverse.ru/esorochinskiy/linion&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;GitVerse&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="75"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_aboutdialog.h" line="88"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="configdialog.ui" line="17"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="322"/>
        <source>Application Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="114"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="327"/>
        <source>App Paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="129"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="329"/>
        <source>Archivers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="166"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="332"/>
        <source>TESO Addons And Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="175"/>
        <location filename="configdialog.ui" line="191"/>
        <location filename="configdialog.ui" line="234"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="333"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="334"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="338"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="201"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="335"/>
        <source>Saved Vars Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="211"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="336"/>
        <source>Backup Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="221"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="337"/>
        <source>TESO Addons Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="241"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="339"/>
        <source>Backup Saved Vars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="280"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="340"/>
        <source>Backup Archivers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="289"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="341"/>
        <source>Do not use just copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="296"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="342"/>
        <source>Tar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="306"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="343"/>
        <source>Zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="319"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="344"/>
        <source>Extractors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="340"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="345"/>
        <source>Zip Extract Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="350"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="346"/>
        <source>Tar Extract Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="91"/>
        <source>Choose ESO Adddons Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="103"/>
        <source>Choose Backup Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="130"/>
        <source>Choose Saved Vars Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="259"/>
        <source>Linion Addon Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="81"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="265"/>
        <source>Backup All Installed Addons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="267"/>
        <source>Backup Installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="122"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="268"/>
        <source>Search For Addons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="270"/>
        <source>Installed Addon List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="135"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="272"/>
        <source>Installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="155"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="274"/>
        <source>Find More Addons To Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="276"/>
        <source>Find More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <location filename="mainwindow.ui" line="175"/>
        <location filename="mainwindow.ui" line="324"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="278"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="280"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="282"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="316"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="281"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="336"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="260"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="261"/>
        <source>Application Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="262"/>
        <source>About this application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="351"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="263"/>
        <source>Show Terminal Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="27"/>
        <location filename="mainwindow.cpp" line="302"/>
        <source>Installed Addons List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="36"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="39"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="40"/>
        <location filename="mainwindow.cpp" line="305"/>
        <source>Reinstall Or Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="41"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="42"/>
        <source>Visit Web site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="56"/>
        <source>Processing: %p%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="111"/>
        <source>Addons available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="114"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="177"/>
        <source>Local Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="177"/>
        <source>Remote Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="181"/>
        <source>Version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="182"/>
        <source>Author: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="199"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="200"/>
        <source>Unable to locate addons. Do you want to open the settings dialog and choose the path to the addons folder yourself?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <location filename="mainwindow.cpp" line="253"/>
        <source>All completed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QAddonListModel</name>
    <message>
        <location filename="QAddonListModel.cpp" line="87"/>
        <source>Not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="89"/>
        <source>Installed, Not backed up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="91"/>
        <source>Installed. Backed up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="177"/>
        <source>No description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="178"/>
        <source>Unknown Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="179"/>
        <source>Unknown Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="181"/>
        <source>Unknown Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="218"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="269"/>
        <location filename="QAddonListModel.cpp" line="275"/>
        <source>Updating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="326"/>
        <location filename="QAddonListModel.cpp" line="342"/>
        <location filename="QAddonListModel.cpp" line="369"/>
        <location filename="QAddonListModel.cpp" line="466"/>
        <location filename="QAddonListModel.cpp" line="683"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="327"/>
        <source>Do you really want to delete this addon: %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="343"/>
        <source>Do you want to make a backup of all installed addons?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="352"/>
        <source>Backing up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="370"/>
        <source>Do you want to backup this addon: %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="378"/>
        <location filename="QAddonListModel.cpp" line="380"/>
        <source>Backing up single addon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="467"/>
        <source>Do you want to restore this addon: %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="475"/>
        <location filename="QAddonListModel.cpp" line="477"/>
        <source>Restoring single addon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="617"/>
        <location filename="QAddonListModel.cpp" line="645"/>
        <location filename="QAddonListModel.cpp" line="710"/>
        <location filename="QAddonListModel.cpp" line="752"/>
        <source>Fatal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="629"/>
        <location filename="QAddonListModel.cpp" line="641"/>
        <source>Processing downloaded data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="646"/>
        <source>Invalid data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="655"/>
        <source>Updating data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="682"/>
        <source>install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="682"/>
        <source>update or reinstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="684"/>
        <source>Do you really want to %1 this addon: %2?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="711"/>
        <source>I/O error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="760"/>
        <location filename="QAddonListModel.cpp" line="795"/>
        <source>Downloading from site</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="31"/>
        <source>TESO addon manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="32"/>
        <source>Created by: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="36"/>
        <source>Usage %1 [options]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="38"/>
        <source>Valid options:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="39"/>
        <source>show this help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="40"/>
        <source>display version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="45"/>
        <source>Unknown option %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="46"/>
        <source>Launch %1 -h or %1 --help for help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="50"/>
        <source>Launch this application without any parameters to see its main functional</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Already Running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="85"/>
        <source>Linion Already Running</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QvObjectDelegate</name>
    <message>
        <location filename="QvObjectDelegate.cpp" line="64"/>
        <source>Version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QvObjectDelegate.cpp" line="74"/>
        <source>Created by: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QvObjectDelegate.h" line="36"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QvObjectDelegate.h" line="37"/>
        <source>Update Available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerminalWindow</name>
    <message>
        <location filename="terminalwindow.ui" line="14"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_terminalwindow.h" line="77"/>
        <source>Terminal Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="terminalwindow.ui" line="54"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_terminalwindow.h" line="78"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
