<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="17"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_aboutdialog.h" line="76"/>
        <source>About This Application</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="23"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_aboutdialog.h" line="77"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans Fallback&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This application is a lightweight addon manager for TESO. Simply a bit faster than its Java big brother. Created and tested on Ubuntu Linux for those playing TESO using WINE.&lt;br /&gt;Author: @esorochinskiy&lt;br /&gt;Special thanks to:&lt;br /&gt;Good Game Mods, LLC&lt;br /&gt;&lt;br /&gt;The software is distributed under &lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;GPL V3&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Source code is available on &lt;a href=&quot;https://github.com/darthtenebrus/linion&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;GitHub&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans Fallback&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This application is a lightweight addon manager for TESO. Simply a bit faster than its Java big brother. Created and tested on Ubuntu Linux for those playing TESO using WINE.&lt;br /&gt;Special thanks to:&lt;br /&gt;Good Game Mods, LLC&lt;br /&gt;&lt;br /&gt;The software is distributed under &lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;GPL V3&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Source code is available on &lt;a href=&quot;https://github.com/darthtenebrus/linion&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;GitHub&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans Fallback&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Это приложение является более легковесным вариантом известного менеджера аддонов TESO. Чуть побыстрее своего Java-собрата. Собран и протестирован под Ubuntu Linux для тех, кто играет в TESO под WINE.&lt;br /&gt;&lt;br /&gt;Автор: @esorochinskiy&lt;br /&gt;Благодарности:&lt;br /&gt;Good Game Mods, LLC&lt;br /&gt;&lt;br /&gt;ПО распространяется под лицензией &lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;GPL V3&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Исходный текст доступен на GitHub &lt;a href=&quot;https://github.com/darthtenebrus/linion&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;GitHub&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="71"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_aboutdialog.h" line="84"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="configdialog.ui" line="17"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="282"/>
        <source>Application Settings</source>
        <translation>Настройки приложения</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="114"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="287"/>
        <source>App Paths</source>
        <translation>Пути</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="129"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="289"/>
        <source>Archivers</source>
        <translation>Архивы</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="166"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="292"/>
        <source>TESO Addons And Backup</source>
        <translation>Аддоны и резервная копия</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="175"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="293"/>
        <source>TESO Addons Path</source>
        <translation>Аддоны TESO</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="191"/>
        <location filename="configdialog.ui" line="214"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="294"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="296"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="198"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="295"/>
        <source>Backup Path</source>
        <translation>Резервные копии</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="253"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="297"/>
        <source>Backup Archivers</source>
        <translation>Архивация</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="262"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="298"/>
        <source>Do not use just copy</source>
        <oldsource>Do not use, just copy</oldsource>
        <translation>Не использовть, только копировать</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="269"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="299"/>
        <source>Tar</source>
        <translation>Архиватор tar</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="279"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="300"/>
        <source>Zip</source>
        <translation>Архиватор Zip</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="292"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="301"/>
        <source>Extractors</source>
        <translation>Распаковщики</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="310"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_configdialog.h" line="302"/>
        <source>Zip Extract Command</source>
        <translation>Распаковщик ZIP</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="78"/>
        <source>Choose ESO Adddons Directory</source>
        <translation>Папка с аддонами ESO</translation>
    </message>
    <message>
        <location filename="configdialog.cpp" line="88"/>
        <source>Choose Backup Directory</source>
        <translation>Папка для резервных копий</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="254"/>
        <source>Linion Addon Manager</source>
        <translation>Менеджер аддонов Linion</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="81"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="259"/>
        <source>Backup All Installed Addons</source>
        <translation>Сделать резервные копии всех установленных аддонов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="261"/>
        <source>Backup Installed</source>
        <oldsource>Backup Installed Addons</oldsource>
        <translation>Сохранить установленные</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="122"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="262"/>
        <source>Search For Addons</source>
        <translation>Поиск Аддонов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="264"/>
        <source>Installed Addon List</source>
        <translation>Список уже установленых аддонов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="135"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="266"/>
        <source>Installed</source>
        <translation>Установленные</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="155"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="268"/>
        <source>Find More Addons To Install</source>
        <translation>Поиск аддонов для установки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="270"/>
        <source>Find More</source>
        <translation>Найти еще</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="316"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="275"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="344"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="257"/>
        <source>About this application</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <location filename="mainwindow.ui" line="175"/>
        <location filename="mainwindow.ui" line="324"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="272"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="274"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="276"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="334"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="255"/>
        <source>About Qt</source>
        <translation>О программе Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="339"/>
        <location filename="obj-x86_64-linux-gnu/linion_autogen/include/ui_mainwindow.h" line="256"/>
        <source>Application Settings</source>
        <translation>Настроить приложение</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="28"/>
        <location filename="mainwindow.cpp" line="281"/>
        <source>Installed Addons List</source>
        <translation>Список установленых аддонов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="37"/>
        <source>Backup</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="101"/>
        <source>Addons available</source>
        <translation>Доступные аддоны</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="38"/>
        <location filename="mainwindow.cpp" line="284"/>
        <source>Reinstall Or Update</source>
        <oldsource>Reinstall/update</oldsource>
        <translation>Переустановить или обновить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="39"/>
        <source>Uninstall</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="40"/>
        <source>Visit Web site</source>
        <translation>Перейти на сайт</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="53"/>
        <source>Processing: %p%</source>
        <translation>Обработка: %p%</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="104"/>
        <source>Install</source>
        <translation>Установить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>Local Description</source>
        <translation>Описание из файла установки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>Remote Description</source>
        <translation>Описание с сайта</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="169"/>
        <source>Version: %1</source>
        <translation>Версия: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="170"/>
        <source>Author: %1</source>
        <translation>Автор: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="187"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Unable to locate addons. Do you want to open the settings dialog and choose the path to the addons folder yourself?</source>
        <translation>Не могу найти аддоны. Хотите открыть диалог конфигурации и указать местоположение папки с аддонами?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>All completed</source>
        <translation>Завершено</translation>
    </message>
</context>
<context>
    <name>QAddonListModel</name>
    <message>
        <location filename="QAddonListModel.cpp" line="196"/>
        <source>Refresh</source>
        <translation>Обновить список</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="157"/>
        <source>No description</source>
        <translation>Нет описания</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="158"/>
        <source>Unknown Author</source>
        <translation>Автор неизвестен</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="159"/>
        <source>Unknown Title</source>
        <translation>Название неизвестно</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="161"/>
        <source>Unknown Version</source>
        <translation>Версия неизвестна</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="243"/>
        <location filename="QAddonListModel.cpp" line="249"/>
        <source>Updating</source>
        <translation>Обновление</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="295"/>
        <location filename="QAddonListModel.cpp" line="311"/>
        <location filename="QAddonListModel.cpp" line="541"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="296"/>
        <source>Do you really want to delete this addon: %1?</source>
        <oldsource>Do you really want to delete this addon: %1</oldsource>
        <translation>Вы действительно хотите удалить аддон %1?</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="312"/>
        <source>Do you want to make a backup of all installed addons?</source>
        <translation>Вы хотите создать резервную копию установленных аддонов?</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="321"/>
        <source>Backing up</source>
        <translation>Сохранение</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="337"/>
        <location filename="QAddonListModel.cpp" line="339"/>
        <source>Backing up single addon</source>
        <translation>Сохранение одного аддона</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="487"/>
        <location filename="QAddonListModel.cpp" line="499"/>
        <source>Processing downloaded data</source>
        <translation>Обработка загруженных данных</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="475"/>
        <location filename="QAddonListModel.cpp" line="503"/>
        <location filename="QAddonListModel.cpp" line="566"/>
        <location filename="QAddonListModel.cpp" line="619"/>
        <source>Fatal</source>
        <translation>Критическая ошибка</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="504"/>
        <source>Invalid data</source>
        <translation>Некорректные данные</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="513"/>
        <source>Updating data</source>
        <translation>Обновление данных</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="540"/>
        <source>install</source>
        <translation>установить</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="540"/>
        <source>update or reinstall</source>
        <translation>обновить или переустановить</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="542"/>
        <source>Do you really want to %1 this addon: %2?</source>
        <translation>Вы действительно хотите %1 аддон %2?</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="567"/>
        <source>I/O error</source>
        <translation>Ошибка ввода/вывода</translation>
    </message>
    <message>
        <location filename="QAddonListModel.cpp" line="627"/>
        <location filename="QAddonListModel.cpp" line="659"/>
        <source>Downloading from site</source>
        <translation>Загрузка с сайта</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="49"/>
        <source>Already Running</source>
        <translation>Уже Запущено</translation>
    </message>
    <message>
        <location filename="main.cpp" line="50"/>
        <source>Linion Already Running</source>
        <translation>Приложение Linion уже запущено</translation>
    </message>
</context>
<context>
    <name>QvObjectDelegate</name>
    <message>
        <location filename="QvObjectDelegate.cpp" line="64"/>
        <source>Version: %1</source>
        <translation>Версия: %1</translation>
    </message>
    <message>
        <location filename="QvObjectDelegate.cpp" line="74"/>
        <source>Created by: %1</source>
        <translation>Создан: %1</translation>
    </message>
    <message>
        <location filename="QvObjectDelegate.h" line="36"/>
        <source>Install</source>
        <translation>Установить</translation>
    </message>
    <message>
        <location filename="QvObjectDelegate.h" line="37"/>
        <source>Update Available</source>
        <translation>Доступно обновление</translation>
    </message>
</context>
</TS>
